﻿-- DROP statements
--DROP TABLE tag_count;
--DROP TABLE top_rep;
--DROP TABLE tags_with_date;


-- 15. and 16.

SELECT tag_id, COUNT(tag_id) 
INTO tag_count
FROM posts_tags
GROUP BY tag_id;


-- 17. and 18.

SELECT *
INTO top_rep
FROM users
JOIN (
	SELECT MAX(reputation)
	FROM users
) max_rep ON users.reputation=max_rep.max;


-- 19. and 22.

SELECT posts.post_id, tag_id, year, month, day
INTO tags_with_date
FROM posts_tags
JOIN posts ON posts.post_id=posts_tags.post_id;
