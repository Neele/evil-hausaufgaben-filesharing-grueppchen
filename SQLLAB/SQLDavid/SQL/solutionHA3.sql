﻿-- 1.
SELECT MIN(age) FROM users;

-- 2.
WITH counter AS (
	SELECT post_history_type_id AS t_id, COUNT(*) 
	FROM post_history
	WHERE year=2013 
	GROUP BY t_id
)
SELECT t_id
FROM (
	SELECT MAX(count)
	FROM counter
) a
JOIN counter c ON a.max = c.count;

-- 3.
SELECT COUNT(id)
FROM (
	SELECT id, COUNT(post_id)
	FROM users u
	FULL JOIN posts p ON u.id = p.owner_user_id
	GROUP BY id
) A
WHERE count=0;

-- 4.
SELECT COUNT(*)
FROM users
WHERE location='Canada';

-- 5.
SELECT title, post_id, score
FROM posts
JOIN (
	SELECT MAX(score)
	FROM posts
	WHERE post_type = 1
) finder ON posts.score = finder.max;

-- 6.
SELECT title
FROM posts
JOIN (
	SELECT post_id
	FROM post_history
	WHERE year=2010 AND post_history_type_id=10
) closed ON posts.post_id=closed.post_id;

-- 7.
SELECT COUNT(*)
FROM posts
WHERE accepted_answer=-1;

-- 8.
SELECT title
FROM posts
WHERE accepted_answer>0;

-- 9.
SELECT AVG(count)
FROM posts
FULL JOIN (
	SELECT parent_id, COUNT(*)
	FROM posts
	WHERE parent_id>0
	GROUP BY parent_id
) answer_count ON posts.post_id=answer_count.parent_id
WHERE post_type=1;

-- 10.
SELECT COUNT(*)
FROM posts_tags
JOIN (
	SELECT post_id
	FROM posts
	JOIN (
		SELECT MAX(score)
		FROM posts
		WHERE post_type=1
	) popular ON posts.score=popular.max
) question ON posts_tags.post_id=question.post_id;

-- 11.
SELECT tag_name, id
FROM tags
JOIN (
	SELECT tag_id
	FROM posts
	JOIN posts_tags ON posts_tags.post_id=posts.post_id
	WHERE accepted_answer>0
) a ON tags.id=a.tag_id
GROUP BY tag_name, id;

-- 12.
SELECT location, display_name
FROM users
WHERE age>32;

-- 13.
SELECT COUNT(*)
FROM (
	SELECT title_edit.post_id
	FROM (
		SELECT post_id
		FROM post_history
		WHERE post_history_type_id=4
	) title_edit
	JOIN (
		SELECT post_id
		FROM post_history
		WHERE post_history_type_id=5
	) body_edit ON title_edit.post_id=body_edit.post_id
	GROUP BY title_edit.post_id
) edit;

-- 14.
WITH counter AS (
	SELECT user_id, COUNT(*)
	FROM badges
	GROUP BY user_id
)
SELECT *
FROM users
JOIN (
	SELECT *
	FROM counter
	JOIN (
		SELECT MAX(count)
		FROM counter
	) bdg ON counter.count=bdg.max
) nerd ON users.id=nerd.user_id;

-- 15.
SELECT tag_name
FROM tags
JOIN (
	SELECT tag_id
	FROM tag_count
	JOIN (
		SELECT MAX(count)
		FROM tag_count
	) pop_tag ON tag_count.count=pop_tag.max
) pop ON tags.id=pop.tag_id;

-- 16.
SELECT tag_name
FROM tags
JOIN (
	SELECT tag_id
	FROM tag_count
	JOIN (
		SELECT MAX(count)
		FROM tag_count
		JOIN (
			SELECT tag_id
			FROM posts_tags
			JOIN (
				SELECT post_id
				FROM posts
				WHERE owner_user_id=101
			) chosen_one ON posts_tags.post_id=chosen_one.post_id
		) chosen_tags ON tag_count.tag_id=chosen_tags.tag_id
	) max_tag ON tag_count.count=max_tag.max
) tag ON tags.id = tag.tag_id;

-- 17.
SELECT display_name, age
FROM top_rep;

-- 18.
SELECT tag_name
FROM tags
JOIN (
	SELECT tag_id
	FROM posts_tags
	JOIN (
		SELECT post_id
		FROM posts
		JOIN top_rep ON top_rep.id=posts.owner_user_id
	) top_rep_posts ON posts_tags.post_id=top_rep_posts.post_id
	GROUP BY tag_id
) top_rep_tags ON tags.id=top_rep_tags.tag_id;

-- 19.
SELECT tag_name
FROM tags
JOIN (
	SELECT tag_id, COUNT(id)
	FROM tags_with_date
	FULL JOIN (
		SELECT tag_id AS id
		FROM tags_with_date
		WHERE year=2011
		GROUP BY id
	) tags_used ON tags_used.id=tags_with_date.tag_id
	GROUP BY tag_id
) tags_used_count ON count=0 AND tags.id=tags_used_count.tag_id;

-- 20.
SELECT COUNT(*)
FROM users
FULL JOIN comments ON users.id = comments.user_id
WHERE comments.id IS NULL;

-- 21.
SELECT posts_per_year.year , post_count, tag_count
FROM (
	SELECT year, COUNT(*) AS post_count
	FROM posts
	GROUP BY year
) posts_per_year
JOIN (
	SELECT year, COUNT(tag_id) AS tag_count
	FROM (
		SELECT year, tag_id
		FROM tags_with_date
		GROUP BY year, tag_id
	) tags_by_year
	GROUP BY year
) tag_count_by_year ON posts_per_year.year = tag_count_by_year.year
ORDER BY year;

-- 22.
SELECT display_name, location
FROM users
JOIN (
	SELECT user_id, COUNT(*)
	FROM users
	JOIN comments ON users.id=comments.user_id
	GROUP BY user_id
) count_comments ON count_comments.user_id=users.id
WHERE count>100;