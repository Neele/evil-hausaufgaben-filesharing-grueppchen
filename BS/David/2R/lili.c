//stdlib needed for malloc, free
#include <stdlib.h>
//stdio needed for printf in print_lili
#include <stdio.h> 
//header
#include "lili.h"

//head of the linkedlist
element_t *head;

/* inserts a new element at the end of the list
*  value:  data for the new element
*  return: value if insertion was succesfully else 0
*/
unsigned int insert_element(unsigned int value){
    //if list is empty
    if (head == NULL){
        //create new element
        //get memory
        head = (element_t*) malloc(sizeof(element_t));
        //if malloc was not succesful
        if (head == NULL){
            //return with error
            return 0;
        }
        //else set data and pointer
        head->data = value;
        head->next = NULL;
        //return withour error
        return value;
    }
    //if list is not empty
    element_t *new = head;
    //search last element
    while (new->next != NULL){
        new = new->next;
    }
    //create new element
    //get memory
    new->next = (element_t*) malloc(sizeof(element_t));
    //if malloc was not succesful
    if (new->next == NULL){
        //return with error
        return 0;
    }
    //else set data and pointer
    new->next->data = value;
    new->next->next = NULL;
    //return withour error
    return value;
}

/* removes the first element of the list
*  return: if list is not empty value of the element removed, else 0
*/
unsigned int remove_element(void){
    //if list is empty
    if (head == NULL){
        //nothing to remove
        return 0;
    }
    //if list is not empty
    //save data and pointer
    element_t *tmpp = head;
    unsigned int tmpv = head->data;
    //set new head
    head = head->next;
    //remove/free first element
    free(tmpp);
    //return value of removed element
    return tmpv;
}

/* prints the linkedlist to stdout
*/
void print_lili(void){
    printf("print_lili: ");
    //if list is not empty
    if (head != NULL){
        //print first element
        element_t *tmp = head;
        printf("%d", tmp->data);
        tmp = tmp->next;
        //print next elements
        while (tmp != NULL){
            printf(", %d", tmp->data);
            tmp = tmp->next;
        }
    }
    printf("\n");
}
