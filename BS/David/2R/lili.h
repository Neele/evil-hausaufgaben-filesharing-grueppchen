#ifndef _LILI_H_
#define _LILI_H_

//element of a linkedlist
typedef struct element 
{
    //data of the element
	unsigned int    data;
    //pointer to the next element (NULL if last element)
	struct element *next;
}element_t;

/* inserts a new element at the end of the list
*  value:  data for the new element
*  return: value if insertion was succesfully else 0
*/
unsigned int insert_element(unsigned int value);

/* removes the first element of the list
*  return: if list is not empty value of the element removed, else 0
*/
unsigned int remove_element(void);

/* prints the linkedlist to stdout
*/
void print_lili(void);

#endif
