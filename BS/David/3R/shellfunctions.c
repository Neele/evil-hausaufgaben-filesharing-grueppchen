#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <limits.h>
#include <sys/wait.h>


#include "plist.h"
#include "shellutils.h"
#include "shellfunctions.h"

//write your local helper functions here

int write_jobs(pid_t pid, const char * command_line){
	printf("[%d] ", pid);
	printf(command_line);
	printf("\n");
	return 0;
}

void read_input(char * const command_line, int size, FILE *stream){
    prompt();
    if (fgets(command_line, size, stream) == NULL && feof(stream)){
        printf("\nMay the force be with you!\n"); 
        exit(0);
    }
    //printf("\n%s\n", command_line);   
}


void execute_command (char * command_line){
    command_t *command = parse_command_line(command_line);
	if (command == NULL || command->parse_error != NULL) {
		if (strcmp(command->command_line,"")) {
			return;
		}
	//} else if (strcmp(command->argv[0],"cd") && &argv[0] != NULL &argv[1] == NULL) {
		
	} else {
		// find out how many arguments command->argv has
		int counter = 0;
		int len;
		int total = strlen(command->command_line);
		if (strchr(command->command_line, '&') != NULL) {
				total--;
		}
		if (strchr(command->command_line, '>') != NULL) {
				total -= 2;
		}
		while (total > 0) {
			len = strlen(command->argv[counter]);
			total = total - len - 1;
			counter++;
		}
		
		// check if cd
		if (!strcmp(command->argv[0], "cd") && counter == 2) {
			if (chdir(command->argv[1])) {
				perror(command->command_line);
			}
			return;
		}
		
		// check if jobs
		if (!strcmp(command->argv[0], "jobs") && counter == 1) {
			walk_list(*write_jobs);
			return;
		}

		pid_t pid = fork();
		int status;

		if (pid == 0) {
			// Fehlerabfangen und mit exit beenden
			if (execvp(command->argv[0], command->argv)) {
				perror(command->command_line);
				free(command);
				exit(-1);
			}
		} else if (!command->background) {
			waitpid(pid, &status, 0);
			print_status(command->command_line, status);
		} else {
			insert_element(pid, command->command_line);	
		}
	}
	free(command);
}


void collect_defunct_process () {
	int status;
	char* buffer = malloc(1024*sizeof(char));
	pid_t child_pid = waitpid(-1, &status, WNOHANG);
	while (child_pid > 0) {
		remove_element(child_pid, buffer, 1024);
		print_status(buffer, status);
		child_pid = waitpid(-1, &status, WNOHANG);
	}
}

