#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <limits.h>
#include <sys/wait.h>

#include "plist.h"
#include "shellutils.h"
#include "shellfunctions.h"

int main () {
    char command_line[COMMAND_LINE_MAXIMUM];
    while (1) {
        collect_defunct_process();
        read_input(command_line, COMMAND_LINE_MAXIMUM, stdin);
        execute_command(command_line);
    }
}
